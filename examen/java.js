//4-----------------------------------
var arr = new Array();
console.log("pregunta4: "+ arr)
//5----------------------------------
let x = "4"+4+5;
let y = 4+4+"5";
console.log("pregunta 5: "+x+" "+y);
//6----------------------------------
function bar (foo){
    return foo &&  foo*3 || null;
}
console.log("pregunta 6: "+bar(true));

//7-------------------------------------
console.log("pregunta 7: "+["H","e"].concat(["i","i","o"]));

//9---------------------------------------------------------------
let transportes = ["Avion","Barco","Motocicleta","Automovil"];
transportes.shift();
console.log("Pregunta 9: "+ transportes);

//10---------------------------------------------------------------
console.log("Preunta 10: "+["W","o","r","i","d"].indexOf("o"));

//11 y 24----------------------------------------------------------------
/* function getname(){
    const name = 'juan';
    name = 'jose';
    return name;
}

getname(); */

//14-------------------------------------------------------------------
function foo(){
    return 5;
}
var myVar = foo;
console.log("Pregunta 14: " + myVar);

//15--------------------------------------------------------------------
(function f(f){
    return console.log("Pregunta 15: " + typeof f());
})(function(){return 1;});

//16--------------------------------------------------------------------
console.log("Pregunta 16: "+ typeof null );

//17--------------------------------------------------------------------
var squqre = number => number * number;
console.log("Pregunta 17: "+squqre(2))

//19-------------------------------------------------------------
var x1 =3;
var y1 =3;

if (x1===y1){
    console.log("pregunta 19: hola mundo");
}

//23-------------------------------------------------------------------
let letras = ['lamda','alfa','gama','beta'];
console.log("Pregunta 23: "+letras.sort());

//26-------------------------------------------------------------------
console.log("pregunta 26:");
[1,2,3,4,5].forEach(function(i){return console.log(i)});

//27-----------------------------------------------------------------------
let mascontas = ['perro', 'gato', 'loro', 'canario'];
mascontas.filter(mascota => mascota.length >=5);
console.log("Pregunta 27: "+mascontas);

//28------------------------------------------------------------------------
function f1(x, y=2, z=7){
    return x+y+z;
}
console.log("prgunta 28: ");
console.log(f1(1)===10);

