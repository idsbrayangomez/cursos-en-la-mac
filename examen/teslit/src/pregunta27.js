import { LitElement , html} from "lit-element";
export class Pregunta27 extends LitElement{
    static get properties(){
        return{frutas:{type:Array}};
    }
    constructor(){
        super();
        this.frutas = ['uva','pera','arandano','manzana','naranja'];
    }
    sortList(e){
        let temporal = [... this.frutas.sort()]; this.frutas = temporal;
        //this.frutas = [... this.frutas.sort()];
        //let temporal = this.frutas.sort(); this.frutas = temporal;
        //this.frutas.sort();this.performUpdate();
    }
    render(){
        return html`
            <ul>${this.frutas.map(f=>html`<li>${f}</li>`)}</ul>
            <button @click = "${this.sortList}">Ordenar</button>
        `;
    }
}
window.customElements.define('pregunta-27',Pregunta27);