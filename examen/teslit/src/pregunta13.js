import { LitElement,html} from "lit-element";

export class Pregunta13 extends LitElement{
    static get properties(){
        return{
            label:{type: String},
            uid:{type: String}
        }
    }
    render(){
        return html `
            <input type="checkbox" id="${this.uid}"> ${this.label}
        `;
    }
}

window.customElements.define('pregunta-13',Pregunta13);