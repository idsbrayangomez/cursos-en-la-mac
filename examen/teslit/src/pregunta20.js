import { LitElement, html } from "lit-element";

export class Pregunta20 extends LitElement{
    static get properties(){
        return{
            numbers:{type:Array}
        };
    }
    constructor(){
        super();
        this.numbers = [1,2,3,4,5];
    }
    handelClick(e){
        console.log('Origin: ',e.composedPath()[0]);
    }
    render(){
        return html`
            ${this.numbers.map(n=> html`
                <button id="button-${n}" @click="${this.handelClick}">${n}</button>
            `)}
        `;
    }
}
window.customElements.define('pregunta-20',Pregunta20);