import { LitElement, html, css } from 'lit-element';
export class Pregunta5 extends LitElement{
    static get properties(){
        return{
            names:{type: Array}
        }
    }
    render(){
        return html`
            <ul>${this.names.map(n=> html`<li>${n}</li>`)}</ul>
        `;
    }
}

window.customElements.define('pregunta-5',Pregunta5);