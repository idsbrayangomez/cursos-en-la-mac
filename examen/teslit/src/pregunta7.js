import { LitElement ,html,css} from "lit-element";
export class Pregunta7 extends LitElement{
    static get properties(){
        return{
            hidden: {type : Boolean},
            state: {type: Number}
        }
    }

    constructor(){
        super();
        this.hidden = false;
        this.state = 0;
    }

    handleClick(e){
        this.hidden = !this.hidden;
        this.state += ( 1 + this.hidden);
    }

    handleClock(e){
        this.hidden =!this.hidden;
        this.state += ( 2 + this.hidden);
    }

    render(){
        return html`
            <p>valos de state: ${this.state} </p>
            ${this.hidden ?
                html`<button @click="${this.handleClick}">cambiar botón true</button>`
                : html`<button @click="${this.handleClock}">cambiar botón false</button>`
            }
        `;
    }

}

window.customElements.define('pregunta-7',Pregunta7);
