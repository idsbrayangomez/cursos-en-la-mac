import { LitElement ,html,css} from "lit-element";

export class Pregunta9 extends LitElement{
    static get properties(){
        return{clicks:{type:Number}};
    }
    constructor(){
        super();
        this.clicks = 0;
    }
    counter(e){
        this.clicks++;
        console.log(this.clicks+2);
    }
    render(){
      return html`
            <button @click="${this.counter}">click me</button>
            <p>click counter: ${this.clicks}</p>
        `;
    }
}

window.customElements.define('pregunta-9',Pregunta9);