import { LitElement , html} from "lit-element";
export class Pregunta23 extends LitElement{
    static get properties(){
        return{
            myProperty:{type: String, value: 'juan'}
        }
    }
    render(){
        return html`
            <p>hola ${this.myProperty}</p>
        `;
    }
}
window.customElements.define('pregunta-23',Pregunta23);