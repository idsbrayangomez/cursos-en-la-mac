import { LitElement, html, css } from 'lit-element';
import { Pregunta4 } from './pregunta4';
export class Pregunta25 extends LitElement{
    static get properties(){
        return{
            number:{type: Number},
            manager:{type: Boolean},
            types:{type:Number},
            counter:{type:Number}
        };
    }
    constructor(){
        super();
        this.number = 1;
        this.manager = true;
        this.types =1;
        this.counter = 0;
    }
    render(){
        return html`
            <h1>Number: ${this.number}</h1>
            <button @click="${this.evento}">Contador</button>
        `;
    }
    evento(e){
        if(this.manager){
            this.number *= this.types;
        }else{
            this.number *= this.types;
        }
        if(this.counter % 2){
            this.types *=3;
        }else{
            this.types -=1;
        }
        this.counter ++;
        this.manager = !this.manager;
    }
}
window.customElements.define('pregunta-25',Pregunta25);