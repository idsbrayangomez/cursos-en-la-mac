import { LitElement , html} from "lit-element";
export class Pregunta26 extends LitElement{
    static get properties() {
        return {
          age: { type: Number },
          name: { type: String },
        };
      }
      render() {
        return html`
          <p>
            ${this.name} es ${!(this.age >= 18) ? 'mayor de edad' : 'menor de edad'}
          </p>
        `;
      }
}
window.customElements.define('pregunta-26',Pregunta26);