import { LitElement, html, css } from 'lit-element';
export class Pregunta4 extends LitElement{
    static get properties(){
        return {
            myProperty:{
                type: Number,
                hasChanged(newValue,oldValue){
                    if (newValue % 2 ===0){
                        return newValue *2;
                    }
                    else{
                        return newValue;
                    }
                }
            }
        }
    }
    constructor(){
        super();
        this.myProperty=47;
    }
    numero(evento){
        this.myProperty = Math.floor((Math.random()*(20+1)));
        console.log("numero de la propiedad"+this.myProperty);
    }
    render(){
        return html `
            <h1>my component</h1>
            <span>${this.myProperty}</span>
            <button @click="${this.numero}">Valor</button>
        `;
    }
}

window.customElements.define('pregunta-4',Pregunta4);