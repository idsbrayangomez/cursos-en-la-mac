import{LitElement,html}from "lit-element"

export class  Pregunta24 extends LitElement{
    static get properties(){
        return{
            counter:{type: Number},
        };
    }
    constructor(){
        super();
        this.counter = 4;
    }
    render(){
        return html`
            <button @click="${this.evento}">Multiplicar</button>
            <p>Valor final: ${this.counter}</p>
        `;
    }
    evento(e){
        this.counter *=2
    }
}
window.customElements.define('pregunta-24',Pregunta24);