import { LitElement , html} from "lit-element";

export class Pregunta21 extends LitElement{
    static get properties(){
        return{
            title:{type:String},
        };
    }
    constructor(){
        super();
        this.title = "Este es mi componente"
    }
    render(){
        return html`
            <p>${this.title}</p>
        `;
    }
    createRenderRoot(){
        return this;
    }
}

window.customElements.define('pregunta-21',Pregunta21);