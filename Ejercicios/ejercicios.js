//este archivo contiene 8 ejercicios 

/* var anObjecto = { 
    foo: 'bar',
    length: 'interesting',
    '0': 'Zero',
    '1': 'one'
}

var anArray = ['zero','one'];
//inprime el valor de la pocicion 0 de anArray
//y inprime el valo del indice 0 del objeto  
console.log(anArray[0],anObjecto[0]);
console.log(anArray[1],anObjecto[1]);
//imprime el tamaño del arreglo y imprimer la propiedad length del objeto 
console.log(anArray.length,anObjecto.length);
//imprimer la propiedad del objeto foo y quiere imprimir 
//algo que no esta definido 
console.log(anArray.foo,anObjecto.foo);
//Compara de que tipo es y devuelve true si es verdad 
console.log(typeof anArray == 'object', typeof anObjecto=='object');
//Compara de que tipo es y devuelve true si es verdad
console.log(anArray instanceof Object, anObjecto instanceof Object);
console.log(anArray instanceof Array, anObjecto instanceof Array);
//
console.log(Array.isArray(anArray),Array.isArray(anObjecto)); 
 */
//-------------------------ejercicio2----------------------------
/* 
var obj={
    a:"hello",
    b:"this is",
    c:"javascript!"
}
//toma como referencias las llaver del objeto
var array= Object.keys(obj);
console.log(array);
 */
//--------------------------ejercicio3----------------------------
/* 
function numero (num){
    for(let i=0;i<=num;i++){
        console.log(i);
    }
}
numero (98);
 */
//----------------------------ejercicio4-------------------------
/* 
let zero = 0;
function multiply(x){ return x*2}
function add(a=1 + zero, b=a, c=b+a,d=multiply(c)){
    console.log((a+b+c),d);
}

add(1);
add(3);
add(2,7);
add(1,2,5);
add(1,2,5,10);
 */
//-------------------------------ejercicio5---------------------
/* 
class MyClass{
    constructor(){
        this.names_=[];
    }
    set name(value){
        this.names_.push(value);
    }
    get name(){
        return this.names_[this.names_.length-1];
    }
}

const myClassInstance = new MyClass();

myClassInstance.name = 'joe';
myClassInstance.name = 'Bob';
//se imprime el metodo get name
//que dice que se va aimprimir el arreglo en la posicion 1
console.log(myClassInstance.name);
//names es el arreglo
console.log(myClassInstance.names_);
 */
//-----------------------------ejercicio6----------------------
/* 
const classInstance = new class{
    get prop(){
        return 5;
    }
};
classInstance.prop = 10;
//sale el valor 5 porque esta retornando el metodo get prop
// el cual tiene de retorno un 5 
console.log(classInstance.prop);
 */
//---------------------------ejercicio7--------------------
/* 
class Queue{
    constructor (){
        const list = [];
        this.enqueue = function(type){
            list.push(type);
            return type;
        };

        this.enqueue = function(){
            return list.shift();
        }
    }
}

var q = new Queue;

q.enqueue(9);
q.enqueue(8);
q.enqueue(7);

console.log(q.enqueue());
console.log(q.enqueue());
console.log(q.enqueue());

//imprime el objeto
console.log(q);
//imprime las keys que tiene el objeto 
console.log(Object.keys(q));
 */

//-------------------------------ejercicio 8-----------------------------

class Person{
    constructor(value1,value2){
        this.fristname = [];
        this.lastname = [];
        this.fristname.push(value1);
        this.lastname.push(value2);
    }
    fristname(value){ 
        this.fristname.push(value);
    }
    lastname(value){
        this.lastname.push(value);
    }
}
let person = new Person('John','Doe');
console.log(person.fristname, person.lastname);
person.fristname = 'Foo';
person.lastname ='Bar';
console.log(person.fristname, person.lastname);