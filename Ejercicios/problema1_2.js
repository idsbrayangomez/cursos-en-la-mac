var personArr=[
    {
        "personId": 123,
        "name": "Jhon",
        "city": "Melbourne",
        "phoneNo":"1234567890"
    },
    {
        "personId": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo":"1234567890"
    },
    {
        "personId": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo":"1234567890"
    },
    {
        "personId": 124,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo":"1234567890"
    }
]

const bodyTable = document.getElementById('tabla');

personArr.forEach((element) => {
    //se crean los elentos de la tabla
    let row = document.createElement('tr');
    
    const tdID = document.createElement('td');
    const tdname = document.createElement('td');
    const tdcity = document.createElement('td');
    const tdtel = document.createElement('td');

    // se le pasan los datos a los elementos de la tabla
    tdID.textContent = element.personId;
    tdname.textContent = element.name;
    tdcity.textContent = element.city;
    tdtel.textContent = element.phoneNo;
    // se amaden al etiqueta tr
    row.appendChild(tdID);
    row.appendChild(tdname);
    row.appendChild(tdcity);
    row.appendChild(tdtel);
    bodyTable.appendChild(row);
});