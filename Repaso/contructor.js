/* class Animal{
    //metodo que se ejecuta primero al hacer un new 
    constructor(){
        console.log("ha nacido un pato");
    }

    static despedir(){
        return 'adios';
    }
    
    //metdos
    hablar(){
        return 'Cuak';
    } 

}
//se ejecuta el constructor 
var pato = new Animal();
//se ejecuta el metodo hablar de la clase 
console.log(pato.hablar());

//se ejecuta el metodo desperir sin crear un objeto de la clase 
console.log(Animal.despedir()); */

class clase{
    constructor(){
        this.name = 'luis';
        console.log('constructor ' + this.name);
    }
    metodo(){
        console.log('metodo: ' + this.name);
    }
}

var c = new clase();

c.name;
c.metodo();