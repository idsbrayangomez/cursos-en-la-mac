class forma{
    constructor(){
        console.log('soy una figura geometrica');
    }
    //metodos
    gritar(){
        console.log('YEP!!');
    }
}

class Cadrado extends forma{
    constructor(){
        // se manda a llamar al constructor de la clase padre
        super();
        console.log('soy un cuadrado');
    }
}

var cuadro = new Cadrado();

cuadro.gritar();