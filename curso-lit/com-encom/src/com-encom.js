import { html, css,LitElement } from 'lit-element';
import './e-contact.js';
import './numero-con';



export class ComEncom extends LitElement {

    static get styles() {
        return css`
          div {
            border: 1px solid black;
            padding: 10px;
            border-radius: 5px;
            display: inline-block;
          }
        `;
      }
  static get properties() {
    return {
      contacts: { type: Array }
    };
  }

  constructor() {
    super();
    this.contacts = [
			{
				nombre: 'Juan Perez',
				correo: 'juanito@gmail.com',
                calle: 'camino a sanvartolo',
                numeroIN: '3S',
                numeroEX: '67',
                tel:2233445566
			},
			{
				nombre: 'Maria Benitez',
				correo: 'MariBeni@gmail.com',
                calle: 'venecia',
                numeroIN: '30S',
                numeroEX: '67J',
                tel:5532279187
			},
			{
				nombre: 'Pedro Calvario',
				correo: 'petercalva@gmail.com',
                calle: 'napoles',
                numeroIN: '45H',
                numeroEX: '4H',
                tel:1234567890
			}
		];
  }

  render() {
    return html`
      <div>
        ${this.contacts.map(contact => {
          return html`
          <div>
            <ul>
                <li>
                    <e-contact
                    name= "${contact.nombre}"
                    email= "${contact.correo}"
                    Calle= "${contact.calle}"
                    numIN = "${contact.numeroIN}"
                    numEX= "${contact.numeroIN}"
                    >
                    </e-contact>
                    <numero-con telefono =${contact.tel}></numero-con>
                </li>
          </ul>
          </div>

          `;
        })}
      </div>
    `;
  }
}

customElements.define('com-encom', ComEncom)