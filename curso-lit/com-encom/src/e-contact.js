import { html, css, LitElement } from 'lit-element'
import './persona-domicilio'

export class eContact extends LitElement {
  static get styles() {
    return css`
      div {
        border: 1px solid black;
        padding: 10px;
        border-radius: 5px;
        display: inline-block;
      }
      h1 {
        font-size: 1.2rem;
        font-weight: normal
      }
    `;
  }


  static get properties() {
    return {
      name: { type: String },
      email: { type: String },

      Calle: { type: String },
      numIN: { type: String },
      numEX:{type:String},

      seeMore: { type: Boolean },
      option: { type: String }
    }
  };

  constructor() {
    super();
    this.name = '';
    this.email = '';
    this.Calle = '';
    this.numIN = '';
    this.numEX = '';
    this.seeMore = false;
    this.option = 'Ver Mas';
  }

  toggle() {
    this.seeMore = !this.seeMore;
    this.seeMore ? this.option = 'Ver menos' : this.option = 'Ver Mas'
  }

  render() {
    return html`
    <div>
      <h2>Daros personales</h2>
      <h1>${this.name}</h1>
      <p><a href="#" @click="${this.toggle}">${this.option}</a></p>

      ${this.seeMore ? html`Email: ${this.email}
      <persona-domicilio Calle="${this.Calle}" numIN ="${this.numIN}" numEX="${this.numEX}" ></persona-domicilio>
      ` : ''}
      </div>
    `;
  }
}

customElements.define('e-contact', eContact);