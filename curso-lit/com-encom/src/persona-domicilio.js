import { html,LitElement } from 'lit-element'

export class PersonaDomicilio extends LitElement {

  static get properties() {
    return {
      Calle: { type: String },
      numIN: { type: String },
      numEX:{type:String},
      seeMore: { type: Boolean },
      option: { type: String }
    }
  };

  constructor() {
    super();
    this.Calle = '';
    this.numIN = '';
    this.numEX = '';
    this.seeMore = false;
    this.option = 'Ver Mas';
  }

  toggle() {
    this.seeMore = !this.seeMore;
    this.seeMore ? this.option = 'Ver menos' : this.option = 'Ver Mas'
  }

  render() {
    return html`
    <div>
      <h2>Domicilio</h2>
      <h1>${this.name}</h1>
      <p><a href="#" @click="${this.toggle}">${this.option}</a></p>

      ${this.seeMore ? html`Calle: ${this.Calle} Numero interior: ${this.numIN} Numero exterior: ${this.numEX}` : ''}
    </div>
    `;
  }
}

customElements.define('persona-domicilio',PersonaDomicilio);