import { html,LitElement } from 'lit-element'

export class NumeroCon extends LitElement {

  static get properties() {
    return {
      telefono: { type: String },
       }
  };

  constructor() {
    super();
    this.telefono = 0;
    }


  render() {
    return html`
    <div>
      <h2>Telefono de contacto</h2>
      ${this.telefono}
    </div>
    `;
  }
}

customElements.define('numero-con',NumeroCon);