import { LitElement, html } from 'lit-element';

export class MisPropiedades extends LitElement {

	static get properties(){
		// console.info('properties')
		return {
			prop1: { type: String },
			prop2: { type: Number },
			prop3: { type: Boolean },
			prop4: { type: Array },
			prop5: { type: Object },
			myProp: { type: Number, attribute: 'my-prop' },
			reflectProp: { type: String, reflect: true },
			access: { type: Number, noAccessor: true },
			changed: { type: Number,
				hasChanged(newVal, oldVal) {
					if (newVal > oldVal) {
						// console.log(`${newVal} > ${oldVal}. hasChanged: true.`);
						return true;
					}
					else {
						// console.log(`${newVal} <= ${oldVal}. hasChanged: false.`);
						return false;
					}
				}
			},
			converter: {
        type: String,
				reflect: true,
				converter(value) {
					console.log('Processing:', value, typeof(value));
					let retVal = Number(value);
					console.log('Returning:', retVal, typeof(retVal));
					return retVal;
				}},
		}
	}

	firstUpdated(changedProperties){
		// console.info('first Updated')
	}

	// // Accessors
	// set access(val) {
	// 	console.info('access',val)
  //   let oldVal = this._access;

	// 	console.info('oldval', oldVal)
  //   this._access = Math.floor(val);

	// 	console.info('this._access', this._access)
  //   this.requestUpdate('access', oldVal);
  // }

  // get access() { return this._access; }

	constructor(){
		// console.info('constructor')
		super();
		this.prop1 = '';
		this.prop2 = null;
		this.prop3 = false;
		this.prop4 = [];
		this.prop5 = {};
		this.reflectProp = 'reflect';
		this.access = 0;
		this.changed = 0;
		this.converter = 'Converter';
	}

	// ref(a,s,d){
	// 	console.info('name',a)
	// 	console.info('old',s)
	// 	console.info('new',d)
	// }

	// //reflect
	// attributeChangedCallback(name, oldval, newval) {
  //   // console.info('attribute change old: ', oldval);
  //   // console.info('attribute change new: ', newval);
	// 	// console.info('name ', name);
	// 	this.ref(name, oldval, newval);
  //   super.attributeChangedCallback(name, oldval, newval);
  // }

	//reflect
	changeProperty(e) {
    let randomString = Math.floor(Math.random()*100).toString();
		// console.info(e.target);
    this.reflectProp = 'myProp ' + randomString;
  }

	//changed
	updated(){
    // console.log('updated');
    // console.info('updated',this.converter)
  }
  getNewVal(){
    let newVal = Math.floor(Math.random()*10);
    this.changed = newVal;
  }

	// //Converter
	changeAttributes() {
    let randomString = Math.floor(Math.random()*100).toString();
		console.info('randomString', randomString)
		console.info('typeof', typeof(randomString))
    this.setAttribute('converter', randomString);
    this.requestUpdate();
  }

  changeProperties() {
    let randomString = Math.floor(Math.random()*100).toString();
		console.info('change', typeof(this.converter))
    this.converter = 'converter ' + randomString;
  }

	render() {
		// console.info('render')
		return html`
			<br>
			<p>${this.reflectProp}</p>
			<button @click="${this.changeProperty}">change property</button>
			<br>
			<p>prop: ${this.access}</p>
      <button @click="${() =>  { this.access = Math.random()*10; }}">
        change prop
      </button>
			<br>
			<p>${this.changed}</p>
      <button @click="${this.getNewVal}">get new value</button>
			<br>
			<p>converter: ${this.converter}</p>

      <button @click="${this.changeProperties}">change properties</button>
      <button @click="${this.changeAttributes}">change attributes</button>
		`;
	}
}

customElements.define('mis-propiedades', MisPropiedades);