import { LitElement, html, css } from 'lit-element';

export class MiCom extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 0;
  }



  render() {
    return html`
      <h1>${this.title} Nr. ${this.counter}!</h1>
      <button part="button" @click=${this._onClick}>increment</button>
      click counter:${this.counter}
      <slot></slot>
    `;
  }

  _onClick() {
    this.counter++;
  }
}

customElements.define('mi-com', MiCom);

