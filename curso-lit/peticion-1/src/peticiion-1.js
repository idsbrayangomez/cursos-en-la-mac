import { LitElement ,css,html} from "lit-element";

export class peticion1 extends LitElement{
    static get styles(){
        return css`
            #tabla{border: solid rebeccapurple}
            th{border: solid rebeccapurple}
            td{border: solid rebeccapurple}
        `;
    }

    static  get properties(){
        return {
            datosp: {typeof:  Array},
            id:{typeof: Number},
            url:{typeof:String},
            verifica:{typeof: Boolean}
        }
    }

    constructor(){
        super();
        this.url='https://jsonplaceholder.typicode.com/posts';
        this.datosp=[];
        this.id=0;
        this.verifica = false;
        this.peticion();
    }

    buscar(e){
        let valor1 = this.renderRoot.getElementById('buscar');
        this.id = valor1.value;
        this.verifica = true;
    }

    peticion(){
        fetch(this.url)
        .then(respuesta=>{
           return respuesta.json()}
        )
        .then(datos=>{
            this.datosp = datos;
        }
        )
        .catch(eror=>{
            console.log(eror);
        });
    }

    borrar(e){
        let valor1 = this.renderRoot.getElementById('buscar');
        valor1.value = '';
        this.verifica = false;
    }

    datoFiltrado(valor,datos){
        if(valor<=100 || valor<=1){
            for (let i = 0; i <= datos.length; i++) {
                if(valor==datos[i].id){
                    datos = datos[i];
                }
            }
        }
        else{
            alert('Registro no encontrado');
        }
        return html`
            <tr id="contenido">
                <td id="id">${datos.id}</td>
                <td id="nombre">${datos.title}</td>
                <td id="mensaje">${datos.body}</td>
            </tr>
        `;
    }

    mostardatos(){

       return this.datosp.map(n=> html`
             <tr id="contenido">
                <td id="id">${n.id}</td>
                <td id="nombre">${n.title}</td>
                <td id="mensaje">${n.body}</td>
            </tr>
        `)
    }

    render(){
        return html`
            <div>
                <label> busqueda por id: </label>
                <input type="text" id="buscar">
                <button id="botonbuscar" @click="${this.buscar}">buscar</button>
                <button id="borrar" @click="${this.borrar}" >borrar</button>
            </div>
            <div>
                <table id="tabla">
                    <tr>
                        <th>id</th>
                        <th>nombre</th>
                        <th>mensaje</th>
                    </tr>
                    ${this.verifica ? this.datoFiltrado(this.id,this.datosp) : this.mostardatos()}
                </table>
            </div>
        `;
    }
}
window.customElements.define('peticion-1',peticion1);