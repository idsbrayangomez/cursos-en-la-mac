import { css } from 'lit-element';
import { HerenciaCss } from './herencia-css';

class MyStyle extends HerenciaCss {
  static get styles() {
    return [
      super.styles,
      css`button { color: red; }`
    ];
  }
}

customElements.define('my-style', MyStyle);
