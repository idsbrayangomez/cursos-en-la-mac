import { LitElement, html, css } from 'lit-element';
import{buttonStyles} from './module-style';

export class CompartirEstilos extends LitElement{
    static get styles(){
        return[
            buttonStyles,
            css`:host{
                display: block;
                border: 1px solid black;
            }`
        ]
    }
    render(){
        return html`
            <button class="blue-button">click</button>
            <button class="blue-button" disabled>no click</button>
        `;
    }
}

window.customElements.define('compartir-estilos',CompartirEstilos);