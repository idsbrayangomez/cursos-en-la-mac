import{html,css,LitElement}from'lit-element';
export class ShadowTreeStyle extends LitElement{
    static get styles(){
        return css`
            *{color:red}
            p{font-family: sans-serif}
            .myclass{margin: 100px}
            #main{padding:30px}
            h1{font-size: 4em}
        `;
    }

    render(){
        return html`
            <P>Con origen en el latín textus, la palabra texto describe a un conjunto de enunciados que permite dar un mensaje coherente y ordenado, ya sea de manera escrita o a través de la palabra. Se trata de una estructura compuesta por signos y una escritura determinada que da espacio a una unidad con sentido.</P>
            <p class="myclass">Parrafo1</p>
            <p id="main">Parrafo2</p>
            <h1>T I T U LO</h1>
        `;
    }
}
customElements.define('shadow-treestyle',ShadowTreeStyle);