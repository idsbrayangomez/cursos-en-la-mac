import{html,css,LitElement} from 'lit-element';
export class HostStyle  extends LitElement{
    static get styles(){
        return css`
            /*selecion del host*/
            :host{
                display: block;
                background-color: azure;
            }
            :host([hidden]){
                display: none;
            }
            :host(.blue){
                background-color: aliceblue;
                color: blue;
            }
            p{
                font-family: fantasy;
            }
        `;
    }

    render(){
        return html`
            <P>Con origen en el latín textus, la palabra texto describe a un conjunto de enunciados que permite dar un mensaje coherente y ordenado, ya sea de manera escrita o a través de la palabra. Se trata de una estructura compuesta por signos y una escritura determinada que da espacio a una unidad con sentido.</P>
        `;
    }
}
customElements.define('host-style',HostStyle);