import { LitElement, html } from 'lit-element';
	
	export class IntroLitElement extends LitElement {
		render() {
			return html`
				<p>Intro Lit element</p>
			`;
		}
	}

customElements.define('intro-litelement',IntroLitElement);