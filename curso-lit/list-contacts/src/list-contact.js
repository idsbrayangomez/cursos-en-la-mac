import { html, css, LitElement } from 'lit-element';
import './e-contact.js';


export class ListContact extends LitElement {
  static get styles() {
    return css`
      #propiedad1{
        color: red;      }
    `;
  }
  static get properties() {
    return {
      contacts: { type: Array },
      data: { type: String },
      count: { type: Number },
      //IMPORTAR 2 COMPONENTES APARTE DEL PRINCIPAL
      // 5 DATOS A MANIPULAR
      content: {type: String},
      identificador: { type: String},
      disabled: { type: Boolean}
    };
  }

  constructor() {
    super();
    this.contacts = [
			{
				nombre: 'Juan Perez',
				correo: 'juanito@gmail.com',
			},
			{
				nombre: 'Maria Benitez',
				correo: 'MariBeni@gmail.com'
			},
			{
				nombre: 'Pedro Calvario',
				correo: 'petercalva@gmail.com'
			}
		];
    this.data = '';
    this.count = 0;
    this.content = 'Contenido';
    this.identificador = 'propiedad1';
    this.disabled = false;
  }


  toggleDisabled() {
    this.disabled = !this.disabled;
    this.content = 'cambiando valor desde evento'
  }

  doChange(event) {
    console.info(event.target);
    this.disabled = event.target.checked;
  }

  dataHandler(event) {
    console.info(event)
    console.info(event.detail)
    this.content = event.detail.nombre;
  }


  render() {
    return html`
      <div>
        ${this.contacts.map(contact => {
          return html`
          <ul>
            <li>
              <e-contact
                name="${contact.nombre}"
                email="${contact.correo}"
                @my-event="${this.dataHandler}"
              ></e-contact>
              ${this.data}
            </li>
          </ul>
          `;
        })}
      </div>
      <br>
      <div
        id="${this.identificador}"
        class="class-name"
      >${this.content}</div>
      <br>
      <button ?disabled="${this.disabled}">BOTON</button>
      <button @click="${this.toggleDisabled}">BOTON secundario</button>
      ${this.content}
      <br>
      <input id="id" type="checkbox" ?checked="${this.disabled}" @change="${this.doChange}">Check

    `;
  }
}

customElements.define('list-contact', ListContact)
