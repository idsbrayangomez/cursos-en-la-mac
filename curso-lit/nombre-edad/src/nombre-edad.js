import {LitElement, html, css} from 'lit-element'

export class NombreEdad extends LitElement{
    static get styles() {
        return css`
          :host {
            display: block;
            border: solid 1px gray;
            padding: 16px;
            max-width: 800px;
          }
        `;
    }

    static get properties() {
        return {
          name: { type: String },
          count: { type: Number },
          alumnos: { type: Array },
          myBool: { type: Boolean },
          valor:{type:Number},
        };
    }

    constructor() {
        super();
        this.myBool = true;
        this.alumnos = [
          {name: 'Pedro', edad: 23},
          {name: 'Antonio', edad: 33},
          {name: 'Juan', edad: 10},
          {name: 'Julio', edad: 67},
          {name: 'Octavio', edad: 22},
          {name: 'Maria', edad: 26},
          {name: 'Julia', edad: 25},
          {name: 'Noelia', edad: 44},
          {name: 'Noe', edad: 30},
          {name: 'Eduardo', edad: 26},
          {name: 'Javier', edad: 29},
          {name: 'Nicolas', edad: 44}
      ];
      this.filtro =[];
    }

    counter() {
        this.myBool = !this.myBool;
        this.filtro = this.alumnos.filter(obj => obj.edad>=25 && obj.edad<=30)
    }



    render() {
        return html`
          <h1>Filtrados de arreglo del intervalo de 25 a 30</h1>
          <button @click="${this.counter}">Filtar</button>
          <div>

          ${this.myBool ?
              this.alumnos.map(datos => html`<li>Nombre:${datos.name} Edad: ${datos.edad}</li>`)
                :
             this.filtro.map(datos => html`<li>Nombre:${datos.name} Edad: ${datos.edad}</li>`)
            }
          </div>

        `;
    }

}

customElements.define('nombre-edad',NombreEdad);