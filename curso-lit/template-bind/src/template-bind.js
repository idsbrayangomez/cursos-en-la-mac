import{LitElement,html,css} from 'lit-element'

export class TemplateBind extends LitElement{
    static get properties(){
        return{
            prop1: {type: String},
            prop2: {type: String},
            prop3: {type: Boolean},
            prop4: {type: String},
            activo:{type:Boolean}
        }

    }

    constructor(){
        super();
        this.prop1 ='tex bindig';
        this.prop2 ='mydiv';
        this.prop3 = true;
        this.prop4 = 'pie';
        this.activo = true;
    }
    render(){
        return html`
            <div>${this.prop1}</div>

            <div id="${this.prop2}">attribute binding</div>

            <div>
                boolean attribute binding
                <input type="text" ?disabled="${this.prop3}">
            </div>
            <div>
                property binding
                <input type= "text" .value="${this.prop4}">
            </div>
            <div>
                property handler binding
                <button @click="${this.clickHander}">Click</button>
            </div>
            <p>
                <input type="checkbox" ?checked="${this.activo}" @change="${this.doCange}">
                check ??
            </p>
        `;
    }
    //funciones de eventos
    clickHander(e){
        console.log(e.target);
    }

    doCange(e){
        console.log("cambia: " + (this.activo = e.target.checked));
    }
}

customElements.define("template-bind",TemplateBind);